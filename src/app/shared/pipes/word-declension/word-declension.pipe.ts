import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'wordDeclension',
})
export class WordDeclensionPipe implements PipeTransform {
  transform(count: number, templates: Array<string>): string {
    const keys: Array<number> = [2, 0, 1, 1, 1, 2];
    const mod: number = count % 100;
    const index: number = mod > 4 && mod < 20 ? 2 : keys[Math.min(mod % 10, 5)];
    return count + ' ' + templates[index];
  }
}
