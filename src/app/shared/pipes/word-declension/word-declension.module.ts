import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WordDeclensionPipe} from './word-declension.pipe';

@NgModule({
  declarations: [WordDeclensionPipe],
  exports: [WordDeclensionPipe],
  imports: [CommonModule],
})
export class WordDeclensionModule {}
