import {Type} from 'class-transformer';

export class TreeNode {
  name: string;

  type: string;

  @Type(() => TreeNode)
  children: TreeNode[];

  isExpand: boolean;

  constructor() {
    this.isExpand = false;
  }
}


