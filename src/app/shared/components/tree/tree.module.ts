import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TreeComponent} from './tree.component';
import {WordDeclensionModule} from '../../pipes/word-declension/word-declension.module';

@NgModule({
  declarations: [TreeComponent],
  exports: [TreeComponent],
  imports: [CommonModule, WordDeclensionModule],
})
export class TreeModule {}
