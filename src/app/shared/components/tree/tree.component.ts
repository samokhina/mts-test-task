import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {TreeNodeTypesEnum} from '../../enums/tree-node-types.enum';
import {TreeNode} from '../../models/tree-node';

const sortTreeNode = (a: TreeNode, b: TreeNode) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0);

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TreeComponent {
  @Input() set setTreeNodes(treeNodes: TreeNode[]) {
    this.treeNodes = this.groupByTreeNodes(treeNodes);
  }
  treeNodes: TreeNode[];
  treeNodeTypes: typeof TreeNodeTypesEnum;

  constructor() {
    this.treeNodes = [];
    this.treeNodeTypes = TreeNodeTypesEnum;
  }

  expandNode(node: TreeNode) {
    node.isExpand = !node.isExpand;
  }

  groupByTreeNodes(treeNodes: TreeNode[]) {
    const folders = treeNodes.filter((node: TreeNode) => this.isFolderTreeNode(node));
    const files = treeNodes.filter((node: TreeNode) => this.isFileTreeNode(node));
    return [...folders, ...files];
  }

  sortTreeNodes(treeNodes: TreeNode[]) {
    const folders = treeNodes.filter((node: TreeNode) => this.isFolderTreeNode(node)).sort(sortTreeNode);
    const files = treeNodes.filter((node: TreeNode) => this.isFileTreeNode(node)).sort(sortTreeNode);
    return [...folders, ...files];
  }

  private isFolderTreeNode(node): boolean {
    return node.type === this.treeNodeTypes.FOLDER;
  }

  private isFileTreeNode(node): boolean {
    return node && node.type === this.treeNodeTypes.FILE;
  }
}
