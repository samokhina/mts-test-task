import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, delay, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {TreeNode} from '../models/tree-node';
import {plainToClass} from 'class-transformer';

@Injectable()
export class DataService {
  url = window.location.origin + `/assets/data.json`;

  constructor(public http: HttpClient) {}

  getTreeNodes(): Observable<TreeNode[]> {
    return this.http.get<{tree: TreeNode[]}>(this.url).pipe(
      delay(1500),
      map((response: {tree: TreeNode[]}) => {
        return response ? plainToClass(TreeNode, response.tree) : [];
      }),
      catchError(() => of([]))
    );
  }
}
