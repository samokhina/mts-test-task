import {Component, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataService} from './shared/services/data.service';
import {TreeNode} from './shared/models/tree-node';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  treeNodes$: Observable<TreeNode[]>;
  loading$: Observable<boolean>;

  private loading: BehaviorSubject<boolean>;

  constructor(private dataService: DataService) {
    this.loading = new BehaviorSubject<boolean>(true);
  }

  ngOnInit(): void {
    this.loading$ = this.loading.asObservable();
    this.treeNodes$ = this.dataService.getTreeNodes().pipe(
      map((treeNodes: TreeNode[]) => {
        this.loading.next(false);
        return treeNodes;
      })
    );
  }
}
